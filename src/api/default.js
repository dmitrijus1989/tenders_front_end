import axios from 'axios'

export async function getTender(id) {
  try {
    const result = await axios.get('http://dc.demopuslapis.online/api/tenders/get_tender/'+id);
    return result;
  } catch (err) {
    return err;
  }
}

export async function getTenders() {
  try {
    const result = await axios.get('http://dc.demopuslapis.online/api/tenders/get_tenders');
    return result;
  } catch (err) {
    return err;
  }
}

export async function postTender(tender) {
  let axiosConfig = {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  };
  try {
    const result = await axios.post('http://dc.demopuslapis.online/api/tenders/post_tender', tender, axiosConfig);
    return result;
  } catch (err) {
    return err;
  }
}

export async function putTender(tender) {
  let axiosConfig = {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  };
  try {
    const result = await axios.put('http://dc.demopuslapis.online/api/tenders/put_tender/'+tender.id, tender, axiosConfig);
    return result;
  } catch (err) {
    return err;
  }
}

export async function deleteTender(id) {
  try {
    const result = await axios.delete('http://dc.demopuslapis.online/api/tenders/delete_tender/'+id);
    return result;
  } catch (err) {
    return err;
  }
}
