import React from 'react';
import history from "./utils/history";
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import routes from "./routes";
import {Container} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from "./components/NavBar.jsx";
import store from './store';

const App = () => {

  const getRoutes = routes => {
        return routes.map((prop, key) => {
          return (
            <Route
              exact={true}
              path={prop.path}
              key={key}
              render={routeProps => <prop.component {...routeProps} />}
            />
          )
        })
  };

  return (
    <Router history={history}>
      <NavBar/>
      <Container mb={10}>
        <Switch>
          {getRoutes(routes)}
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
