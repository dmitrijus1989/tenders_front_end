import Home from "./view/Home";
import NoPage404 from "./view/NoPage404";
import Add from "./view/Add";
import Edit from "./view/Edit";


const routes = [
  {
    path: '/add',
    component: Add
  },
  {
    path: '/edit/:id',
    component: Edit
  },
  {
    path: '/home',
    component: Home
  },
  {
    path: '/',
    component: Home
  },
  {
    path: '/*',
    component: NoPage404
  },
]

export default routes;