
class CustomStore {
  constructor() {
    this.info = {
      success: false,
      successEdit: false
    };
  }

  isSuccess = () => {
    return this.info.success;
  };

  setSuccess = (boolean) => {
    this.info = {
      ...this.info,
      success: boolean
    }
  };

  isSuccessEdit = () => {
    return this.info.successEdit;
  };

  setSuccessEdit = (boolean) => {
    this.info = {
      ...this.info,
      successEdit: boolean
    }
  };
}

const CustomStoreInstance = new CustomStore();

export const {
  isSuccess,
  setSuccess,
  isSuccessEdit,
  setSuccessEdit
} = CustomStoreInstance;

export default CustomStoreInstance;
