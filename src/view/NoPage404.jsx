import React from "react";

const NoPage404 = (props) => {

  const style = {
    margin: 'auto',
    textAlign: 'center'
  }

  return (
    <div style={style}>
      <img src={'/images/404.jpg'} alt={404}/>
    </div>
)
}

export default NoPage404;