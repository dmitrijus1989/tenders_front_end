import React, {useState} from "react";
import {Col, Row, Card, Button, Form} from "react-bootstrap";
import {postTender} from "../api/default";
import store from './../store';
import {useHistory} from 'react-router-dom';

const Add = () => {
  const history = useHistory();

  const defaultState = {
    title: '',
    description: ''
  };
  const [state, setState] = useState(defaultState);
  const [loading, setLoading] = useState(false);

  const style = {
    marginTop: 30
  };

  const submitForm = (e) => {
    e.preventDefault();
    setLoading(true);
    console.log('sent: ', state);
    postTender(state).then(r => {
      store.setSuccess(true);
      setLoading(false);
      console.log('response: ', r);
      history.push('/home');
    })
  };

  if (loading) {
    return <div className={'text-center mt-3'}>Loading...</div>
  }

  return (
    <Row style={style}>
      <Col sm={12}>
        <Card>
          <Card.Header>Add new tender</Card.Header>
          <Card.Body>
            <Form onSubmit={submitForm}>
              <Row>
                <Col sm={12}>
                    <Form.Group controlId="formTitle">
                      <Form.Label>Title</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder=""
                        name={'title'}
                        onChange={e => setState({...state, title: e.target.value})}
                      />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                      <Form.Label>Description</Form.Label>
                      <Form.Control
                        as="textarea"
                        rows="3"
                        name={'description'}
                        onChange={e => setState({...state, description: e.target.value})}
                      />
                    </Form.Group>
                </Col>
              </Row>
              <Button className="float-right" variant="primary" type={'submit'}>Submit</Button>
            </Form>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
};

export default Add;