import React, {useEffect, useState} from "react";
import {Row, Col} from "react-bootstrap";
import ReactTableDefault from "../components/ReactTable";
import {getTenders} from "../api/default";
import {Link} from 'react-router-dom';
import {deleteTender} from "../api/default";
import SuccessAlert from '../components/SuccessAlert';
import store from './../store';

const Home = (props) => {
  const [state, setState] = useState({
    tenders: []
  });
  const [idToDelete, setIdToDelete] = useState(null);
  const [loading, setLoading] = useState(true);
  const [successAlert, setSuccessAlert] = useState({
    show: false,
    message: ''
  });

  useEffect(() => {
    getTenders().then(result => {
      setState({
        tenders: sortData(result.data)
      });
      setLoading(false);
    });
  }, []);

  useEffect( () => {
    if (store.isSuccess()) {
      setSuccessAlert({
        show: true,
        message: 'Tender has successfully created'
      })
      store.setSuccess(false);
    };

    if (store.isSuccessEdit()) {
      setSuccessAlert({
        show: true,
        message: 'Tender has successfully updated'
      })
      store.setSuccessEdit(false);
    }

  }, [store]);

  //remove deleted tenders from state
  useEffect(() => {
    if (!!idToDelete && state.tenders.length > 0) {
      const tempArray = state.tenders;
      const index = tempArray.findIndex(el => el.id === idToDelete);
      if (index >= 0) {
        tempArray.splice(index, 1);
      }
      setState({
        tenders: [...tempArray]
      });
      setIdToDelete(null);
      setLoading(false);
    }
  }, [idToDelete]);

  const deleteTenderById = (id) => {
    setLoading(true);
    deleteTender(id).then(r => {
      setIdToDelete(id);
      setLoading(false);
      setSuccessAlert({
        show: true,
        message: 'Tender has been deleted'
      })
      console.log(r)
    });
  };


  const sortData = data => {
    if (!!data) {
      const result = data.map((prop, key) => {
        return {
          ...prop,
          actions: (
            // we've added some custom button actions
            <div className="actions-right">
              {/* use this button to add a edit kind of action */}
              <Link to={'/edit/' + prop['id']} className={'edit-button mr-2'}>
                  <i className="fa fa-edit" />
              </Link>
              <span className={'delete-button'} onClick={() => deleteTenderById(prop.id)}>
                <i className="fa fa-close" />
              </span>
            </div>
          ),
        };
      });
      return result;
    } else {
      return [];
    }
  };

  const style = {
    marginTop: 30
  };

  if (loading) {
    return <div className={'mt-3 text-center'}>Loading...</div>
  }
  return (
    <React.Fragment>
      <SuccessAlert
        show={successAlert.show}
        close={() => {setSuccessAlert({show: false, message: ''})}}
        message={successAlert.message}
      />
      <Row style={style}>
        <Col sm={12}>
          <ReactTableDefault tenders={state.tenders}/>
        </Col>
      </Row>
    </React.Fragment>
  )
}

export default Home;