import React, {useState, useEffect} from "react";
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import {getTender, putTender} from "../api/default";
import store from './../store';
import {useHistory} from 'react-router-dom';

const Edit = (props) => {

  const history = useHistory();
  const id = props.match.params.id;

  useEffect(() => {
    if (!!id) {
      getTender(id).then(result => {
        const tender = result.data;
        if (!!tender) {
          setState({
            id: !!tender.id ? tender.id :'',
            title: !!tender.title ? tender.title :'',
            description: !!tender.description ? tender.description :'',
          })
          setLoading(true);
        }
      })
    }
  }, [id]);

  const defaultState = {
    id: null,
    title: '',
    description: ''
  };
  const [state, setState] = useState(defaultState);
  const [loading, setLoading] = useState(false);

  const style = {
    marginTop: 30
  };

  const submitForm = (e) => {
    e.preventDefault();
    setLoading(true);
    console.log('sent: ', state);
    putTender(state, id).then(r => {
      setLoading(false);
      store.setSuccessEdit(true);
      console.log('response: ', r)
      history.push('/home')
    })
  };

  if (!loading) {
    return <div className={'text-center mt-4'}>Loading...</div>
  }
  return (
    <Row style={style}>
      <Col sm={12}>
        <Card>
          <Card.Header>Add new tender</Card.Header>
          <Card.Body>
            <Form onSubmit={submitForm}>
              <Row>
                <Col sm={12}>
                  <Form.Group controlId="formTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=""
                      name={'title'}
                      value={state.title}
                      onChange={e => setState({...state, title: e.target.value})}
                    />
                  </Form.Group>
                  <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows="3"
                      name={'description'}
                      value={state.description}
                      onChange={e => setState({...state, description: e.target.value})}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Button className="float-right" variant="primary" type={'submit'}>Submit</Button>
            </Form>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
};

export default Edit;