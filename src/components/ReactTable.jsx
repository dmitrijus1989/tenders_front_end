import React from "react";
import ReactTable from "react-table-6";
import 'react-table-6/react-table.css'

const ReactTableDefault = ({tenders}) => {

  const columns = [{
    Header: 'Title',
    accessor: 'title' // String-based value accessors!
  }, {
    Header: 'Description',
    accessor: 'description',
    Cell: props => <span>{props.value}</span> // Custom cell components!
  }, {
    Header: 'Created',
    accessor: 'created',
    Cell: props => <span className={'text-center'}>{new Date(props.value).toISOString().slice(0, 19).replace('T', ' ')}</span>
  }, {
    Header: 'Actions',
    accessor: 'actions',
    headerClassName: 'actions-header-right',
    sortable: false,
    filterable: false,
  }]


  return (
    <ReactTable
      data={tenders}
      columns={columns}
      headerStyle={{textAlign: 'center'}}
      style={{textAlign: 'center'}}
    />
  )

}

export default ReactTableDefault;