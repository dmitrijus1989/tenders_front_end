import React from "react";
import SweetAlert from 'sweetalert2-react';

const SuccessAlert = ({show, close, message}) => {

  return (
    <SweetAlert
      show={show}
      title="Success"
      text={message}
      onConfirm={() => close()}
    />
  )
};

export default SuccessAlert;