import React from "react";
import {Navbar, Nav} from "react-bootstrap";
import {Link} from "react-router-dom";

const NavBar = () => {

  return (
    <Navbar bg="dark" variant="dark">
      <Link to={'/home'}>
        <Navbar.Brand>
          <img
            alt=""
            src="/logo192.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{' '}
          Tenders
        </Navbar.Brand>
      </Link>

      <Nav className="justify-content-end ml-3">
            <Link to="/add">Add new tender</Link>
      </Nav>
    </Navbar>
  )
}

export default NavBar;