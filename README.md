# React Tenders app

Simple react app

## [DEMO](http://dcfront.demopuslapis.online/)

## Install guide

```
   git clone repository
   go to cloned directory
   run npm install
```


## Enjoy
To start dev server
```
    npm start
```
To creating a Production build
```
    npm run build
```